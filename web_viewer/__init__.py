import os
from flask import Flask


def create_app(development=False):
    app = Flask(__name__, instance_relative_config=True)
    try:
        os.makedirs(app.instance_path)
    except FileExistsError:
        pass

    if development:
        app.config.from_pyfile("developmentconfig.py")
    else:
        app.config.from_pyfile("config.py")

    app.config["DATABASE"] = os.path.join(app.instance_path, "land_use_conc_limit.db")

    from . import search_pages

    app.register_blueprint(search_pages.bp)
    app.add_url_rule("/", endpoint="index")
    return app
