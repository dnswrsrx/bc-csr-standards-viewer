from flask import Blueprint, render_template, request
from web_viewer import database_commands as db
from jinjasql import JinjaSql

j = JinjaSql(param_style="qmark")
bp = Blueprint("search_pages", __name__)


@bp.route("/", methods=["GET", "POST"])
@bp.route("/search", methods=["GET", "POST"])
def index(data=None):
    land_uses = get_table_content("select code, name from land_use;")
    site_specific_factors = get_table_content(
        "select code, factor from site_specific_factor;"
    )
    if request.method == "POST":
        parameters = {
            "chemical": validate_search_parameter(request.form.get("chemical").lower()),
            "land_use": validate_search_parameter(request.form.getlist("land_use")),
            "site_specific_factor": validate_search_parameter(
                request.form.getlist("site_specific_factor")
            ),
        }
        data = land_use_concentration_limit_content(parameters)
    return render_template(
        "search.html",
        land_uses=land_uses,
        site_specific_factors=site_specific_factors,
        data=data,
    )


def land_use_concentration_limit_content(parameters):
    template = """
        select * from land_use_concentration_limit
        {% if chemical or land_use or site_specific_factor %}
        where
        {% if chemical %} chemical = {{ chemical }} {% endif %}
        {% if CAS %} or CAS = {{ CAS }} {% endif %}
        {% if land_use %}
        {% if chemical and land_use %} and {% endif %}
        land_use in {{ land_use|inclause }}
        {% endif %}
        {% if (chemical or land_use) and site_specific_factor %} and {% endif %}
        {% if site_specific_factor %}
        site_specific_factor in {{ site_specific_factor|inclause }}
        {% endif %}
        {% endif %}
    """
    query, parameters = j.prepare_query(template, parameters)
    parameters = list(parameters)
    data = {
        "headers": get_table_headers(query, parameters),
        "content": get_table_content(query, parameters),
    }
    return data


def validate_search_parameter(parameter):
    if parameter in ["", "--", []]:
        return None
    return parameter


@bp.route("/tables", methods=["GET", "POST"])
def tables(data=None):
    queries = {
        "chemical": "select * from chemical;",
        "land_use_concentration_limit": "select * from land_use_concentration_limit;",
        "land_use": "select * from land_use;",
        "site_specific_factor": "select * from site_specific_factor;",
    }
    if request.method == "POST":
        button_pressed = request.form["table"]
        query = queries[button_pressed]
        data = {
            "headers": get_table_headers(query),
            "content": get_table_content(query),
        }
    return render_template("tables.html", tables=queries.keys(), data=data)


def get_table_headers(query, parameters=None):
    with db.get_db() as connection:
        if parameters:
            headers = connection.cursor().execute(query, parameters)
        else:
            headers = connection.cursor().execute(query)
    return [description[0] for description in headers.description]


def get_table_content(query, parameters=None):
    with db.get_db() as connection:
        if parameters:
            content = connection.cursor().execute(query, parameters)
        else:
            content = connection.cursor().execute(query)
    return content
