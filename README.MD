# Web Viewer Readme

## Using

Firstly, make sure you are in the `web_viewer` directory. Then, hit `flask run` and on your web browser, navigate to (enter in your address bar) `localhost:5000`.

If you get an error on the lines of `FLASK_APP is not defined`:

For Windows: `set FLASK_APP=web_viewer.py`

For Linux: `export FLASK_APP=web_viewer.py`
