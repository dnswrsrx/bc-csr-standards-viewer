from web_viewer import create_app

app = create_app(development=False)

if __name__ == "__main__":
    app.run()
